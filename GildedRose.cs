﻿using System.Collections.Generic;

namespace csharp
{
    public class GildedRose
    {
        IList<Item> Items;
        public GildedRose(IList<Item> Items)
        {
            this.Items = Items;
        }

        public void UpdateQuality()
        {
            for (var i = 0; i < Items.Count; i++)
            {
                int sellInnCount = 1;
                int qualityCount = 1;
                int qualityCoeff = -1;


                // Once the sell by date has passed, Quality degrades twice as fast.
                if (Items[i].SellIn < 0)
                {
                    qualityCount = 2;
                }
                // "Conjured" items degrade in Quality twice as fast as normal items.
                if (Items[i].Name == "Conjured Mana Cake")
                {
                    qualityCount = 2;
                }
                else
                { 
                    // "Sulfuras", being a legendary item, never has to be sold or decreases in Quality.
                    if (Items[i].Name == "Sulfuras, Hand of Ragnaros")
                    {
                        sellInnCount = 0;
                        qualityCount = 0;
                    }
                    else
                    {
                        if (Items[i].Name == "Aged Brie")
                        {
                            // "Aged Brie" actually increases in Quality the older it gets.
                            qualityCoeff = 1;
                        }
                        else
                        {
                            if (Items[i].Name == "Backstage passes to a TAFKAL80ETC concert")
                            {
                                // Quality drops to 0 after the concert.
                                if (Items[i].SellIn <= 0)
                                {
                                    Items[i].Quality -= Items[i].Quality;
                                    qualityCoeff = 0;
                                }
                                else
                                {
                                    // Quality increases by 3 when there are 5 days or less
                                    if (Items[i].SellIn <= 5)
                                    {
                                        qualityCount = 3;
                                    }
                                    else
                                    {
                                        // Quality increases by 2 when there are 10 days or less
                                        if (Items[i].SellIn <= 10)
                                        {
                                            qualityCount = 2;
                                        }
                                    }
                                    // "Backstage passes", like aged brie, increases in Quality as its SellIn value approaches.
                                    qualityCoeff = 1;
                                }
                            }
                        }
                    }
                }
                Items[i].SellIn -= sellInnCount;
                Items[i].Quality += (qualityCoeff * qualityCount);

                // The Quality of an item is never negative.
                if (Items[i].Quality < 0 )
                {
                    Items[i].Quality = 0;
                }

                // The Quality of an item is never more than 50.
                if (Items[i].Quality > 50 )
                {
                    Items[i].Quality = 50;
                }
            }
        }
    }
}
